# Status
This prject is going to be rewriten for OpenBSD and will be relicenced under a software licence that has yet to be decided. 

# Dotfiles

This repo contains the dot files for the Browse-OS user.

## What is here
- [x] Sway Config
- [x] Waybar Config
- [ ] Waybar CSS
- [x] Start Script
- [x] Bash Profile 
- [x] Clam AV script to scan all downloads

## How to install

Read the code, understand how it works (read the docs on whatever it is you don't), follow the wiki if you need more help.

### Dependencies (most of them anyway)

- wlogout
- bash
- inotify-tools
- sway
- waybar
- clamav
- flatpak
- firefox
- a base system (testing for releses done on fedora w/ selinux enabled)
- whatever the stuff above needs
