#!/bin/bash

# Check for a new file
while read -r file; do
# If the file is there
if [[ -s "${file}" ]]; then
# Scan it via clamscan 
clamscan -r --remove --no-summary -i -l "${HOME}/.ClamAV.log" "${file}"
# Send the last line of the log as a desktop notification
notify-send "$(tail -n 1 "${HOME}/.ClamAV.log")"
# Remove the log
rm ${HOME}/.ClamAV.log
fi
done < <(inotifywait -q -m -e close_write,moved_to --format '%w%f' "${HOME}/Downloads") 
