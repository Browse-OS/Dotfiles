#!/bin/bash
#Autosetup script
clear
read -p "Press enter to setup, this actionn cannot be undone, YOU SHOULD READ AND UNDERSTAAND THIS SCRIPT."
clear
echo "You will need root/wheel access"

#start script
#
#Deps
sudo dnf install wlogout inotify-tools sway waybar clamav flatpak fontawesome-fonts fontawesome5-fonts-all ghc-base-unicode-symbols powerline-fonts google-noto-sans-symbols-fonts google-noto-emoji-fonts rsms-inter-fonts mako ufw

#Remove unneeded packages
sudo dnf remove firewalld

sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo  

flatpak install flathub org.mozilla.firefox

#setup firewall
sudo ufw enable

sudo ufw reset

sudo ufw default deny outgoing

sudo ufw default deny incoming

sudo ufw allow out 80,443/tcp

sudo ufw allow out 53,5353/udp

sudo ufw reload


mv .* ../

